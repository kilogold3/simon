using System;
using UnityEngine;
using System.Collections;
using VRStandardAssets.Utils;

namespace Simon
{
    [ExecuteInEditMode]
    public class SkipSequenceController : MonoBehaviour
    {
        [SerializeField]
        private SelectionRadial radial;
        private VRInteractiveItem vrInteraction;

        public event Action OnSkipSequence;

        /*************************************************************************
         * We add this redundant event execution because the SkipSequenceController can have multiple
         * ways of determining a skip request from the user. It just so happens that the radial 
         * also reports completion using an event, thus making this look like event-respons-to-event.
         * Having the radial's event publicly exposed allows direct coupling of the radial via this
         * class (which would add needless complexity).
         **************************************************************************/ 
        void Awake()
        {
            radial.OnSelectionComplete += radial_OnSelectionComplete;
        }

        void radial_OnSelectionComplete()
        {
            if (OnSkipSequence != null)
                OnSkipSequence();
        }

        
        private void OnEnable()
        {
            radial.Show();
        }

        private void OnDisable()
        {
            radial.Hide();
        }
    }
}
