﻿using UnityEngine;
using System.Collections;

namespace Simon
{
    public class WallMenuButtonQuit : WallMenuButton
    {
        public WallMenuPresenter menuPresenter;

        protected override void Execute()
        {
            menuPresenter.PresentMain();
        }
    }
}
