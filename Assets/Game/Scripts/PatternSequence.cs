﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Simon
{
    public class PatternSequence : MonoBehaviour
    {
        public float stepsDelay;
        private List<PatternElements> elements;

        // Use this for initialization
        void Awake()
        {
            elements = new List<PatternElements>();
        }

        public void AddNewElement( PatternElements element )
        {
            elements.Add(element);
        }

        public void ResetSequence()
        {
            elements.Clear();
            stepsDelay = 0;
        }

        public PatternElements[] GetElements()
        {
            return elements.ToArray();
        }

        public int GetElementCount()
        {
            return elements.Count;
        }
    }
}