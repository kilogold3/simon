﻿using UnityEngine;
using System.Collections;

namespace Simon
{
    public class DestroyOnLoad : MonoBehaviour
    {

        // Use this for initialization
        void Start()
        {
            Destroy(gameObject);
        }
    }
}
