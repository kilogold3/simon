﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace Simon
{    
    public class WallGestureColorize : MonoBehaviour
    {
        public class ColorSpecs
        {
            public Color EmissiveColor      { get; private set; }
            public Color DimColor           { get; private set; }
            public bool IsEmissiveSpecified { get; private set; }
            public bool IsDimSpecified      { get; private set; }

            public ColorSpecs(Color emissiveColor)
            {
                EmissiveColor = emissiveColor;
                IsEmissiveSpecified = true;
                IsDimSpecified = false;
            }

            public ColorSpecs(Color emissiveColor, Color dimColor)
            {
                DimColor = dimColor;
                EmissiveColor = emissiveColor;
                IsEmissiveSpecified = true;
                IsDimSpecified = true;
            }
        }

        //Typedef -- sort of...
        public class ColorConfig : Dictionary<PatternElements, ColorSpecs> { }

        public IlluminatingWall[] illuminatingWalls;

        private ColorConfig originalConfigEmissive = new ColorConfig();
        // Use this for initialization
        void Awake()
        {
            foreach (var wall in illuminatingWalls)
            {
                originalConfigEmissive.Add(wall.elementType, new ColorSpecs( wall.illuminatedEmissiveColor, wall.DimEmissiveColor ));
            }
        }

        public void Colorize(ColorConfig config)
        {
            foreach (var item in config)
            {
                var wall = Array.Find(illuminatingWalls, e => e.elementType == item.Key);
                if (wall)
                {
                    if (item.Value.IsDimSpecified)
                        wall.DimEmissiveColor = item.Value.DimColor;

                    if (item.Value.IsEmissiveSpecified)
                        wall.illuminatedEmissiveColor = item.Value.EmissiveColor;
                }
            }
        }

        public void ResetColors()
        {
            foreach (var wall in illuminatingWalls)
            {
                var wallColorSpecs = originalConfigEmissive[wall.elementType];

                if (wallColorSpecs.IsDimSpecified)
                    wall.DimEmissiveColor = wallColorSpecs.DimColor;

                if (wallColorSpecs.IsEmissiveSpecified)
                    wall.illuminatedEmissiveColor = wallColorSpecs.EmissiveColor;
            }
        }
    }
}