﻿using System;
using System.Collections;
using UnityEngine;
using VRStandardAssets.Utils;

namespace Simon
{
    [RequireComponent(typeof(VRInteractiveItem))]
    public class IlluminatingWall : MonoBehaviour
    {
        public Color illuminatedEmissiveColor;

        [SerializeField]
        private Color dimEmissiveColor;

        private MeshRenderer wallRenderer;

        private Material wallMaterial;
        private readonly string shaderEmissiveName = "_EmissionColor";
        private readonly string shaderKeywordFeatureEmission = "_EMISSION";

        // This value will cause a warning in the editor about the value being animated.
        private const float disabledIntensity = 0.2f;
        private const float enabledIntensity = 1.0f;

        public event Action<PatternElements> OnIlluminate;
        public event Action<PatternElements> OnDim;
        public PatternElements elementType;
        public bool PlaySound = true;
        public bool IsIlluminated { get; private set; }

        /// <summary>
        /// Flag to determine if the illumination came from input.
        /// This is used for when the player's interaction is deactivated while the wall is lit.
        /// Logic will cancel a Dim upon interaction-deactivation if it has been lit by another class.
        /// Logic will cause a Dim upon interaction-deactivation if only illuminated from input.
        /// </summary>
        private bool illuminatedFromInput;

        /// <summary>
        /// Similar to Unity's UI button logic. This allows for the script to be enabled
        /// with still remaining non-interactive.
        /// </summary>
        public bool IsInteractible { get; private set; }

        /// <summary>
        /// Flags whether the wall is currently dimming or not.
        /// When we illuminate a wall, it may have already been dimming.
        /// We will cancel the dim and allow it to illuminate.
        /// </summary>
        public bool IsDimming { get; private set; }

        public Color DimEmissiveColor
        {
            get{ return dimEmissiveColor; }

            set
            {
                dimEmissiveColor = value;
                if( false == IsIlluminated)
                    ColorizeWall(dimEmissiveColor, GetComponent<MeshRenderer>().sharedMaterial);
            }
        }

        // Use this for initialization
        void Awake()
        {
            IsInteractible = true;
            illuminatedFromInput = true;

            wallRenderer = GetComponent<MeshRenderer>();
            wallMaterial = wallRenderer.material;
            wallMaterial.EnableKeyword(shaderKeywordFeatureEmission);

            // Initialize in a dimmed state.
            DimImpl();

            // Assign Input
            GetComponent<VRInteractiveItem>().OnDown += InputIlluminate;
            GetComponent<VRInteractiveItem>().OnUp += InputDim;
            GetComponent<VRInteractiveItem>().OnOut += InputDim;
        }

        public void SetIsInteractible(bool isInteractibleIn)
        {
            IsInteractible = isInteractibleIn;

            if (IsInteractible == false && illuminatedFromInput)
            {
                Dim();
            }
        }

        public void Illuminate()
        {
            illuminatedFromInput = false;
            IlluminateImpl();
        }

        public void Dim()
        {
            illuminatedFromInput = false;
            DimImpl();
        }

        private void InputDim()
        {
            if (IsInteractible)
            {
                illuminatedFromInput = false;
                DimImpl();
            }
        }

        private void InputIlluminate()
        {
            if (IsInteractible)
            {
                illuminatedFromInput = true;
                IlluminateImpl();
            }
        }

        private void IlluminateImpl()
        {
            if (IsDimming)
            {
                StopAllCoroutines();
                IsDimming = false;
            }

            ColorizeWall(illuminatedEmissiveColor);

            if (PlaySound)
                GetComponent<AudioSource>().Play();

            IsIlluminated = true;

            if (OnIlluminate != null)
                OnIlluminate(elementType);
        }

        private void DimImpl()
        {
            if (IsIlluminated)
            {
                StartCoroutine(DimImplCoroutine());
            }
        }

        //TODO:
        // Create a fade out effect to hide the abrupt input-decativation dim.
        private IEnumerator DimImplCoroutine()
        {
            IsDimming = true;

            if (OnDim != null)
                OnDim(elementType);


            //Give some frames to at least see the effect upon ultra-quick Illuminate/Dim.
            yield return null;
            yield return null;
            yield return null;
            yield return null;

            ColorizeWall(DimEmissiveColor);

            IsIlluminated = false;
            IsDimming = false;
        }

        /// <summary>
        /// Intended to apply effect instantly.
        /// For animations, calculate color can call this method
        /// for every loop/step.
        /// </summary>
        /// <param name="color"></param>
        private void ColorizeWall(Color color)
        {
            ColorizeWall(color, wallMaterial);
        }
        private void ColorizeWall( Color color, Material mat)
        {
            mat.SetColor(shaderEmissiveName, color);
            DynamicGI.SetEmissive(wallRenderer, color);
        }
    }
}
