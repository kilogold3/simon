﻿using UnityEngine;
using System.Collections;

public class Highscore : MonoBehaviour 
{
    private const string prefsHighestLevel = "HighestLevel";
    public int HighestLevel
    {
        get { return PlayerPrefs.GetInt(prefsHighestLevel); }
        set { PlayerPrefs.SetInt(prefsHighestLevel,value); }
    }
}
