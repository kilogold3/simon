using UnityEngine;
using System.Collections;

namespace Simon
{
    /// <summary>
    /// The main Game Logic class. No other class should depend on this one.
    /// this class ties the logic of all other components together.
    /// Mediator Pattern
    /// </summary>
    public class GameSession : MonoBehaviour
    {
        public Highscore highscore;
        public PatternSequence sequence;
        public PatternSequenceGenerator seqGenerator;
        public PatternSequencePlayer seqPlayer;
        public PatternSequenceRecorder seqRecorder;
        public PatternSequenceCompare seqCompare;
        public WallGesturePulse wallGesturePulse;
        public WallMenuPresenter menuController;
        public SkipSequenceController seqSkipController;

        public int CurRound { get; private set; }

        public AudioClip soundRoundWin;
        public AudioClip soundRoundLose;
        public AudioClip soundRoundStart;

        public IlluminatingWall[] illuminatingWalls;

        private bool shouldPresentSkip = false;
        private bool isYieldingUntilReady = false;

        void Awake()
        {
            seqPlayer.PlaySequenceEnd += OnPlaySequenceEnd;
            seqRecorder.OnRecordingEnd += OnRecordSequenceEnd;
            seqRecorder.OnNewRecordedElement += OnNewRecordedElement;
            seqSkipController.OnSkipSequence += OnSkipSequence;
        }

        private void OnNewRecordedElement(PatternElements obj)
        {
            bool isMatching = seqCompare.CompareSequenceStepsProgressively(
                seqRecorder.RecordedSequence,
                sequence);

            // If we failed to match, we should end the recording here. No point
            // in making the player continue this round. 
            // Recording-End callback to this class will handle round re-setup.
            if( false == isMatching )
            {
                seqRecorder.EndRecording();
            }
        }

        public void StartNewGame()
        {
            CurRound = 0;
            sequence.ResetSequence();

            // Start at initial round
            AdvanceToNextRound();

            PlayRound();
        }

        public void AdvanceToNextRound()
        {
            ++CurRound;
            highscore.HighestLevel = CurRound;
            seqGenerator.IncreaseDifficulty(sequence);
        }

        public void PlayRound()
        {
            wallGesturePulse.OnGestureComplete += OnPlayRoundPulseGestureEnd;

            const int numPulses = 5;
            const float pulseLength = 0.1f;
            const float secondsBetweenPulses = 0;
            
            SetWallsActiveInteraction(false); // Disable interaction until the sequence has played.
            wallGesturePulse.Pulsate(numPulses, pulseLength, secondsBetweenPulses, Color.gray, soundRoundStart);
        }

        private void OnPlaySequenceEnd()
        {
            if (false == isYieldingUntilReady)
            {
                StartCoroutine(CoYieldUntilReadyToPlay());
            }
            else
            {
                Debug.LogWarning("Attempted to yield when already yielding.");
            }
        }

        private void OnRecordSequenceEnd()
        {
            SetWallsActiveInteraction(false);
            bool didWin = seqCompare.CompareSequenceStepsProgressively(seqRecorder.RecordedSequence, sequence);
            PresentToast( didWin );
        }

        private void PresentToast( bool didWin )
        {
            const int numPulses = 1;
            const float pulseLength = 0.5f;
            const float secondsBetweenPulses = 0;
            const float colorIntensity = 0.6f;
            if( didWin )
            {
                wallGesturePulse.Pulsate(numPulses, pulseLength, secondsBetweenPulses, new  Color(0,colorIntensity,0), soundRoundWin);
            }
            else
            {
                wallGesturePulse.Pulsate(numPulses, pulseLength, secondsBetweenPulses, new Color(colorIntensity,0,0), soundRoundLose);
            }

            wallGesturePulse.OnGestureComplete += OnToastPulseGestureEnd;
        }


        private void SetWallsActiveInteraction( bool isActive )
        {
            if (illuminatingWalls.Length == 0)
                Debug.LogError("No illuminating walls assigned.");
            foreach (var wall in illuminatingWalls)
            {
                wall.SetIsInteractible(isActive);
            }
        }

        private void SetWallsActiveIllumination(bool isActive)
        {
            if (illuminatingWalls.Length == 0)
                Debug.LogError("No illuminating walls assigned.");
            foreach (var wall in illuminatingWalls)
            {
                if (isActive)
                    wall.Illuminate();
                else
                    wall.Dim();
            }
        }

        /// <summary>
        /// Called when toast is displayed.
        /// </summary>
        private void OnToastPulseGestureEnd()
        {
            wallGesturePulse.OnGestureComplete -= OnToastPulseGestureEnd;

            bool didWin = seqCompare.CompareSequenceStepsProgressively(seqRecorder.RecordedSequence, sequence);

            if (didWin)
            {
                AdvanceToNextRound();
                Invoke("PlayRound", 0.5f);
            }
            else
            {
                // When the sequence recording is ended, wall interactions are diabled.
                // This is followed by the toast, which executes a wall-pulse.
                // At this point, the walls are still disabled.
                SetWallsActiveInteraction(true);
                menuController.PresentContinue();
            }
        }

        /// <summary>
        /// Called when the round is about to be played.
        /// This is too verbose & ugly. Consider implementing the following:
        /// http://wiki.unity3d.com/index.php/Event-based_Coroutine        
        /// </summary>
        private void OnPlayRoundPulseGestureEnd()
        {
            wallGesturePulse.OnGestureComplete -= OnPlayRoundPulseGestureEnd;
            StartCoroutine(OnPlayRoundPulseGestureEndCoRoutine());
        }
        private IEnumerator OnPlayRoundPulseGestureEndCoRoutine()
        {
            // Give some time (without interaction) after the pulse effect is given to see where the
            // illumination is.
            yield return new WaitForSeconds(0.6f);

            seqPlayer.PlaySequence(sequence);

            if (true == shouldPresentSkip)
            {
                shouldPresentSkip = false;
                seqSkipController.gameObject.SetActive(true);
            }
        }

        /// <summary>
        /// Called when a skip-sequence request is acknowledge/accepted.
        /// </summary>
        private void OnSkipSequence()
        {
            seqPlayer.EndSequencePlayback();
            SetWallsActiveIllumination(false);
            seqSkipController.gameObject.SetActive(false);
        }

        /// <summary>
        /// Halts game logic until everything is ready to accept
        /// player input.
        /// </summary>
        private IEnumerator CoYieldUntilReadyToPlay()
        {
            isYieldingUntilReady = true;

            // Continue looping until the scene is ready to
            // allow the player to interact (all walls are dimmed).
            loopRestart:
            foreach (var wall in illuminatingWalls)
            {
                if (wall.IsIlluminated)
                {
                    yield return null;
                    goto loopRestart;
                }
            }

            seqSkipController.gameObject.SetActive(false);
            seqRecorder.BeginRecording(sequence.GetElementCount());
            SetWallsActiveInteraction(true);

            isYieldingUntilReady = false;
        }

        /// <summary>
        /// When a player fails, the player may retry the round.
        /// This time, the player is offered to skip the sequence, 
        /// if they feel confident in their memory
        /// </summary>
        public void RetryRound()
        {
            menuController.HideMenu();

            shouldPresentSkip = CurRound > 5;

            PlayRound();
        }
    }
}