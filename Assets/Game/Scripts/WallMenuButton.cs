﻿using UnityEngine;
using System.Collections;
using VRStandardAssets.Utils;

namespace Simon
{
    /// <summary>
    /// This class handles interpretation of VRInteractiveItem events.
    /// Once the class determines that the proper interactions are taking place
    /// to resemble the usage of pressing a button, the button logic is executed.
    /// </summary>
    public abstract class WallMenuButton : MonoBehaviour
    {
        // TODO:
        // Write logic for handling the down & up being within the bounds. 

        public VRInteractiveItem illuminatingWall;

        private void OnEnable()
        {
            illuminatingWall.OnUp += Execute;
        }

        private void OnDisable()
        {
            illuminatingWall.OnUp -= Execute;
        }

        protected abstract void Execute();
    }
}