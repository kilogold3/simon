﻿using UnityEngine;
using System.Collections;

namespace Simon
{
    public class WallMenuPresenter : MonoBehaviour
    {
        public WallGestureColorize wallGestureColorize;
        public Transform MenuRootTransform;
        public Transform ContinueTransform;
        public Transform MainTransform;

        public void PresentContinue()
        {
            const float dimIntensity = 0.3f;
            var colorConfig = new WallGestureColorize.ColorConfig {
                { PatternElements.Right,  new WallGestureColorize.ColorSpecs(Color.green, Color.green * dimIntensity ) },
                { PatternElements.Left, new WallGestureColorize.ColorSpecs( Color.red, Color.red * dimIntensity) }
            };

            wallGestureColorize.Colorize(colorConfig );
            MainTransform.gameObject.SetActive(false);
            ContinueTransform.gameObject.SetActive(true);

            MenuRootTransform.gameObject.SetActive(true);
        }

        public void PresentMain()
        {
            wallGestureColorize.ResetColors();

            MainTransform.gameObject.SetActive(true);
            ContinueTransform.gameObject.SetActive(false);

            MenuRootTransform.gameObject.SetActive(true);
        }

        public void HideMenu()
        {
            wallGestureColorize.ResetColors();
            MenuRootTransform.gameObject.SetActive(false);
        }
    }
}