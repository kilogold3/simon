﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace Simon
{
    public class PatternSequenceRecorder : MonoBehaviour
    {
        public Transform illuminatingWallsRoot;
        public bool isRecording;

        [SerializeField]
        private int stepsToRecord;

        [SerializeField]
        private int stepsRecorded;

        public event Action OnRecordingEnd;
        public event Action<PatternElements> OnNewRecordedElement;

        public PatternSequence RecordedSequence { get; private set; }

        // Use this for initialization
        void Awake()
        {
            RecordedSequence = GetComponent<PatternSequence>();

            var illuminatingWalls = illuminatingWallsRoot.GetComponentsInChildren<IlluminatingWall>();

            foreach (var wall in illuminatingWalls)
            {
                wall.OnDim += Wall_OnDim;
            }
        }

        private void Wall_OnDim(PatternElements obj)
        {
            if (isRecording)
            {
                ++stepsRecorded;
                RecordedSequence.AddNewElement(obj);

                if( OnNewRecordedElement != null)
                {
                    OnNewRecordedElement(obj);
                }

                if(stepsToRecord != -1 && stepsRecorded == stepsToRecord)
                {
                    EndRecording();
                }
            }
        }

        /// <summary>
        /// Begins recording a sequence.
        /// </summary>
        /// <param name="stepsToRecordIn">Number of steps to record. Use -1 if manual or indefinite recording.</param>
        public void BeginRecording( int stepsToRecordIn )
        {
            if( isRecording )
            {
                Debug.LogWarning("Recorder already recording. Recording has been reset.");
            }

            isRecording = true;
            stepsToRecord = stepsToRecordIn;
            stepsRecorded = 0;
            RecordedSequence.ResetSequence();
        }

        public void EndRecording()
        {
            if (isRecording)
            {
                isRecording = false;

                if (OnRecordingEnd != null)
                    OnRecordingEnd();
            }
        }

        public void PlayRecording( float stepsDelay )
        {
            GetComponentInParent<PatternSequencePlayer>().PlaySequence(RecordedSequence, stepsDelay);
        }
    }
}