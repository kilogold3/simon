﻿using UnityEngine;
using System.Collections;
using VRStandardAssets.Utils;

namespace Simon
{
    public class VRInteractiveItemDebugTest : MonoBehaviour
    {
        // Use this for initialization
        void Awake()
        {
            GetComponent<VRInteractiveItem>().OnDown += Fire;
        }

        // Update is called once per frame
        void Fire()
        {
            Debug.Log("VR Hit");
        }
    }
}