using UnityEngine;
using VRStandardAssets.Utils;
using System.Collections;
using System.Collections.Generic;
using System;

namespace Simon
{
    public class PatternSequencePlayer : MonoBehaviour
    {
        private bool isSequencePlaying = false;

        public IlluminatingWall Up;
        public IlluminatingWall Down;
        public IlluminatingWall Left;
        public IlluminatingWall Right;
        public IlluminatingWall Front;
        public IlluminatingWall Back;

        public event Action PlaySequenceEnd;

        public float illuminationToggleDelay;

        public void PlaySequence( PatternSequence sequence )
        {
            if( false == isSequencePlaying)
            {
                StartCoroutine(PlaySequenceCoroutine(sequence.GetElements(), sequence.stepsDelay ));
            }
        }

        public void PlaySequence(PatternSequence sequence, float delayBetweenSteps)
        {
            if (false == isSequencePlaying)
            {
                StartCoroutine(PlaySequenceCoroutine(sequence.GetElements(), delayBetweenSteps));
            }
        }

        public void EndSequencePlayback()
        {
            if (isSequencePlaying)
            {
                //Should only be one coroutine running...
                StopAllCoroutines();

                isSequencePlaying = false;

                if (PlaySequenceEnd != null)
                    PlaySequenceEnd();
            }
        }

        private IEnumerator PlaySequenceCoroutine(PatternElements[] sequenceElements, float delayBetweenSteps )
        {
            isSequencePlaying = true;
            foreach (var element in sequenceElements)
            {
                float curDelayTick = illuminationToggleDelay;
                var curElement = GetCorrespondingElement(element);
                curElement.Illuminate();

                // Better precision at micro-numbers if we calculate ourselves instead of WaitForSeconds().
                while (curDelayTick > 0)
                {
                    curDelayTick -= Time.deltaTime;
                    yield return null;
                }

                curElement.Dim();

                yield return new WaitForSeconds(delayBetweenSteps);
            }
            isSequencePlaying = false;

            if (PlaySequenceEnd != null)
                PlaySequenceEnd();
        }

        private IlluminatingWall GetCorrespondingElement( PatternElements element)
        {
            switch (element)
            {
                case PatternElements.Up:
                    return Up;
                case PatternElements.Down:
                    return Down;
                case PatternElements.Left:
                    return Left;
                case PatternElements.Right:
                    return Right;
                case PatternElements.Front:
                    return Front;
                case PatternElements.Back:
                    return Back;
                default:
                    return null;
            }
        }
    }
}
