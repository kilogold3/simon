﻿using UnityEngine;
using System.Collections;

namespace Simon
{
    public class WallMenuButtonContinue : WallMenuButton
    {
        public GameSession gameSession;

        protected override void Execute()
        {
            gameSession.RetryRound();
        }
    }
}