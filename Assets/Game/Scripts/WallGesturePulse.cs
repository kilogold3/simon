﻿using UnityEngine;
using System.Collections;
using System;

namespace Simon
{
    public class WallGesturePulse : MonoBehaviour
    {
        public IlluminatingWall[] illuminatingWalls;
        private Color[] illuminatingWallsColorCache;
        bool isExecutingCoroutine = false;

        public event Action OnGestureComplete;
        private AudioSource pulseAudio;

        void Awake()
        {
            pulseAudio = GetComponent<AudioSource>();

            illuminatingWallsColorCache = new Color[illuminatingWalls.Length];

            for (int i = 0; i < illuminatingWalls.Length; i++)
            {
                illuminatingWallsColorCache[i] = illuminatingWalls[i].illuminatedEmissiveColor;
            }
        }

        public void Pulsate(int numPulses, float pulseLengthInSeconds, float secondsBetweenPulses, Color pulseColor, AudioClip pulseSound)
        {
            if (!isExecutingCoroutine)
                StartCoroutine(PulsateCoroutine(pulseSound, numPulses, pulseLengthInSeconds, secondsBetweenPulses, true, pulseColor));
        }

        public void Pulsate(int numPulses, float pulseLength, float secondsBetweenPulses)
        {
            if (!isExecutingCoroutine)
            {
                Color dummyColor = Color.white;
                StartCoroutine(PulsateCoroutine(null, numPulses, pulseLength, secondsBetweenPulses, false, dummyColor));
            }
        }


        private IEnumerator PulsateCoroutine(AudioClip pulseSound, int numPulses, float pulseLengthInSeconds, float secondsBetweenPulses, bool specifyColor, Color pulseColor)
        {
            isExecutingCoroutine = true;

            if (pulseSound != null)
                pulseAudio.clip = pulseSound;

            bool[] previousInteractibleState = new bool[illuminatingWalls.Length];
            for (int i = 0; i < illuminatingWalls.Length; i++)
            {
                // Do not play spatialized sound.
                illuminatingWalls[i].PlaySound = false;

                // Cache interactivity state, then disable (we don't want to unintentionally re-enable)
                previousInteractibleState[i] = illuminatingWalls[i].IsInteractible;
                illuminatingWalls[i].SetIsInteractible(false);

                // Switch wall colors (optional).
                if (specifyColor)
                    illuminatingWalls[i].illuminatedEmissiveColor = pulseColor;
            }

            // Perform pulsation
            for (int i = 0; i < numPulses; i++)
            {
                foreach (var wall in illuminatingWalls)
                {
                    wall.Illuminate();
                    pulseAudio.Play();
                }

                yield return new WaitForSeconds(pulseLengthInSeconds);


                foreach (var wall in illuminatingWalls)
                {
                    wall.Dim();
                }

                yield return new WaitForSeconds(secondsBetweenPulses);
            }

            // Restore the wall color and re-enable interactivity 9f applicable).
            for (int i = 0; i < illuminatingWalls.Length; i++)
            {
                if (specifyColor)
                    illuminatingWalls[i].illuminatedEmissiveColor = illuminatingWallsColorCache[i];

                illuminatingWalls[i].PlaySound = true;

                illuminatingWalls[i].SetIsInteractible(previousInteractibleState[i]);
            }

            isExecutingCoroutine = false;

            if (OnGestureComplete != null)
                OnGestureComplete();
        }
    }
}