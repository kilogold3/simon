﻿using UnityEngine;
using System.Collections;

public class CameraManager : MonoBehaviour
{
    public GameObject vrCamera;
    public GameObject stadardCamera;

    // Use this for initialization
    void Awake()
    {
#if !UNITY_EDITOR && UNITY_ANDROID
        Destroy(stadardCamera.gameObject);
#else
        Destroy(vrCamera.gameObject);
#endif
    }
}
