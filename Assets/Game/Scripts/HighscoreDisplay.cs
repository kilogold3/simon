using UnityEngine;
using System.Collections;
using TMPro;

public class HighscoreDisplay : MonoBehaviour 
{
	// Use this for initialization
	void OnEnable () 
	{
        var highscore = GetComponent<Highscore>();
        GetComponent<TextMeshPro>().text = 
            "Personal Best\n\n<size=8>" + highscore.HighestLevel + "</size>";
	}
}
