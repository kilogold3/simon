﻿using UnityEngine;
using System.Collections;

namespace Simon
{
    /// <summary>
    /// Currently designed for NewGame only, as this is the only interaction.
    /// In the future, for more specialized interactions, make an abstract method for button "execution".
    /// This method can be overriden for any specialized behaviour.
    /// </summary>
    public class WallMenuButtonNewGame : WallMenuButton
    {
        public WallMenuPresenter menuPresenter;
        public GameSession gameSession;

        protected override void Execute()
        {
            menuPresenter.HideMenu();
            gameSession.StartNewGame();
        }
    }
}