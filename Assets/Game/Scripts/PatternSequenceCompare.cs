﻿using UnityEngine;
using System.Collections;
using System;

namespace Simon
{
    public class PatternSequenceCompare : MonoBehaviour
    {
        public bool CompareSequenceStepsProgressively(PatternSequence a, PatternSequence b)
        {
            var aElements = a.GetElements();
            var bElements = b.GetElements();

            int commonIndices = Mathf.Min(aElements.Length, bElements.Length);

            for (int i = 0; i < commonIndices; i++)
            {
                if (aElements[i] != bElements[i])
                {
                    return false;
                }
            }

            return true;
        }
    }
}