﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Simon
{
    public enum PatternElements { Up, Down, Left, Right, Front, Back }

    [Serializable]
    public class PatternSequenceGenerator : MonoBehaviour
    {
        public float defaultStepsDelay = 1.4f;
        public float speedIncreaseRate = 0.2f;
        public int levelUpsUntilSpeedIncrease = 3; // How many level-ups until speed increase.


        /// <summary>
        /// Generates a sequence based on the specified difficulty.
        /// </summary>
        /// <param name="sequence">The sequence instance to assemble</param>
        /// <param name="difficultyLevelIn">The difficulty for the sequence.</param>
        /// <param name="retainSequencePattern">If true,</param>
        public void GenerateSequenceDifficulty(PatternSequence sequence, int difficultyLevelIn, bool retainSequencePattern)
        {
            // Reset values
            if (false == retainSequencePattern)
            {
                sequence.ResetSequence();
            }
            sequence.stepsDelay = defaultStepsDelay;

            // Assign new difficulty values
            for (int j = 0; j < difficultyLevelIn; j++)
            {
                // For every 'SpeedRateIncrease' amount...
                if (j % levelUpsUntilSpeedIncrease == 0 && j != 0)
                {
                    IncreaseSpeed(sequence);
                }
                else if (false == retainSequencePattern )
                {
                    IncreaseSequence(sequence);
                }
            }
        }

        public void IncreaseDifficulty( PatternSequence sequence )
        {
            Debug.Log("Increasing difficulty.");
            var elemCount = sequence.GetElementCount();

            // If we are starting with a new sequence...
            if (elemCount == 0) 
            {
                sequence.stepsDelay = defaultStepsDelay;
            }

            // If we are modifying an existing sequence...
            else if (elemCount % levelUpsUntilSpeedIncrease == 0 )
            {
                IncreaseSpeed(sequence);
            }

            IncreaseSequence(sequence);
        }

        private void IncreaseSpeed( PatternSequence sequence )
        {
            Debug.Log("Speed increased.");

            sequence.stepsDelay -= speedIncreaseRate;
            if (sequence.stepsDelay < 0)
                sequence.stepsDelay = 0;
        }

        private void IncreaseSequence( PatternSequence sequence )
        {
            Debug.Log("Sequence increased.");

            sequence.AddNewElement(GenerateRandomElement());
        }

        private PatternElements GenerateRandomElement()
        {
            var values = Enum.GetValues(typeof(PatternElements));
            return (PatternElements)UnityEngine.Random.Range(0, values.Length );
        }
    }
}