﻿using UnityEngine;
using System.Collections;

namespace Simon
{
    public class PatternSequenceDebugInput : MonoBehaviour
    {
        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyUp(KeyCode.S))
            {
                FindObjectOfType<WallGestureColorize>().ResetColors();
            }

            if (Input.GetKeyUp(KeyCode.Q))
            {
            }

            if (Input.GetKeyUp(KeyCode.A))
            {
            }
        }
    }
}
