﻿using UnityEngine;
using UnityEditor;
using System.Collections;

namespace Simon
{
    public class RoomScaler
    {
        const float scaleFactor = 0.1f;

        [MenuItem("Simon's Box/Scale Room Up %PGUP")]
        private static void ScaleRoomUp()
        {
            Transform wallsParent = GameObject.Find("Room/IlluminatingWalls").transform;
            wallsParent.localScale *= 1 + scaleFactor;

            Transform menusParent = GameObject.Find("Room/Menus").transform;
            foreach (Transform subMenus in menusParent)
            {
                foreach (Transform cur in subMenus)
                {
                    cur.localPosition *= 1 + scaleFactor;
                }
            }
        }

        [MenuItem("Simon's Box/Scale Room Up %PGDN")]
        private static void ScaleRoomDown()
        {
            Transform wallsParent = GameObject.Find("Room/IlluminatingWalls").transform;
            wallsParent.localScale *= 1 - scaleFactor;

            Transform menusParent = GameObject.Find("Room/Menus").transform;
            foreach (Transform subMenus in menusParent)
            {
                foreach (Transform cur in subMenus)
                {
                    cur.localPosition *= 1 - scaleFactor;
                }
            }
        }
    }
}