﻿using UnityEngine;
using System.Collections;
using UnityEditor;

namespace Simon
{
    /// <summary>
    /// This class is designed to treat all dim emissive assignments
    /// as one instance. If we modify one emissive, we modify them all.
    /// </summary>
    [CustomEditor(typeof(IlluminatingWall))]
    public class IlluminatingWallEditor : Editor
    {
        private Color previousDimColor;

        public override void OnInspectorGUI()
        {
            var myTarget = (IlluminatingWall)target;

            previousDimColor = myTarget.DimEmissiveColor;

            DrawDefaultInspector();

            // If we are in EDIT MODE [AND]
            // the GUI has been interacted with [AND]
            // the color is different than it was before...
            if( false == Application.isPlaying && 
                true == GUI.changed && 
                previousDimColor != myTarget.DimEmissiveColor)
            {
                // We have modified the color. 
                // Let's apply the same color to all the OTHER walls.
                var walls = FindObjectsOfType<IlluminatingWall>();

                foreach (var item in walls)
                {
                    if (myTarget != item)
                    {
                        item.DimEmissiveColor = myTarget.DimEmissiveColor;
                        EditorUtility.SetDirty(item);
                    }
                }
            }
        }
    }
}