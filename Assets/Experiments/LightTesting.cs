﻿using UnityEngine;
using System.Collections;

public class LightTesting : MonoBehaviour 
{
    private Material mat;
    private readonly string shaderEmissiveName = "_EmissionColor";

    // Use this for initialization
    void Start () 
	{
        mat = GetComponent<MeshRenderer>().material;
    }
	
	// Update is called once per frame
	void Update () 
	{
        if( Input.GetKeyDown(KeyCode.A))
        {
            mat.SetColor(shaderEmissiveName, Color.yellow);
            DynamicGI.SetEmissive(GetComponent<MeshRenderer>(), Color.yellow);


        }
    }
}
